var express = require('express');
var passport = require('passport');
var TwitterStrategy = require('passport-twitter').Strategy;

var user = {
	id : "foo"
};

passport.serializeUser(function(_user, done) {
	done(null, user.id);
});
passport.deserializeUser(function(id, done) {
	done(null, user);
});

var twitterAuthz = new TwitterStrategy({
	consumerKey : "3yNkptsVylZqtVa1nzd6Q",
	consumerSecret : "GaO82ZUiJx0X6TaSTuh1UZovBkAhj00q786zrPO40",
	callbackURL : "http://localhost:8888/auth/twitter/callback",
	userAuthorizationURL : 'https://api.twitter.com/oauth/authenticate'
}, function(token, tokenSecret, profile, done) {

	console.log("token:" + token + ", tokenSecret:" + tokenSecret);
	console.log('Houston, we have a login');
	console.log(profile);
	done(null, user);
});

passport.use(twitterAuthz);

//creamos una aplicacion
var app = express();

//session

app.use(express.methodOverride());
// Passport needs express/connect's cookieParser and session
app.use(express.cookieParser());
app.use(express.session({
	secret : "blahhnsnhoaeunshtoe"
}));
app.use(passport.initialize());
app.use(passport.session());
// Passport MUST be initialize()d and session()d before the router
app.use(app.router);

app.get("/", function(request, response) {
	response.send("hello world");
});

app.get("/login", function(request, response) {
	var html = "<a href=\"/auth/twitter\">Sign in with Twitter</a>";

	response.send(html);
});

app.get('/auth/twitter', passport.authenticate('twitter'));
app.get('/auth/twitter/callback', passport.authenticate('twitter', {
	successRedirect : '/',
	failureRedirect : '/login'
}));

//indicamos que la aplicacion escuche el puerto 8888
app.listen(8888);
