var express = require('express');
var passport = require('passport');
var TwitterStrategy = require('passport-twitter').Strategy;
var Twitter = require("twitter-js-client").Twitter;

var TWITTER_CONSUMER_KEY = "3yNkptsVylZqtVa1nzd6Q";
var TWITTER_CONSUMER_SECRET = "GaO82ZUiJx0X6TaSTuh1UZovBkAhj00q786zrPO40";

//DEFINIMOS LAS FUNCIONES DE ERROR AL CONSULTAR EL API DE TWITTER
function error(err, response, body, requestResponse) {

	requestResponse.send("<b>ERRROR</b>" + JSON.stringify(err));
	//requestResponse.end();
};

function success(data, requestResponse) {

	requestResponse.send(JSON.stringify(data));	
};

function muestraDatos(usuarioAccessToken, usuarioTokenSecret, screen_name, response) {

	var config = {
		"consumerKey" : TWITTER_CONSUMER_KEY,
		"consumerSecret" : TWITTER_CONSUMER_SECRET,
		"accessToken" : usuarioAccessToken,
		"accessTokenSecret" : usuarioTokenSecret,
		"callBackUrl" : "https://127.0.0.1"
	};

	var twitter = new Twitter(config);

	twitter.getUserTimeline({
		screen_name : screen_name,
		count : '10'
	}, function(err, responseR, body) {
		//patron fachada
		error(err, responseR, body, response);
	}, function(data) {
		//patron fachada
		success(data, response);
	});
	/*
	 twitter.getMentionsTimeline({
	 count : '10'
	 }, error, success);
	 twitter.getHomeTimeline({
	 count : '10'
	 }, error, success);
	 twitter.getReTweetsOfMe({
	 count : '10'
	 }, error, success);
	 twitter.getTweet({
	 id : '1111111111'
	 }, error, success);
	 */

}

//guardar datos en session
//solo para usos de BD?
passport.serializeUser(function(user, done) {
	done(null, user);
});

passport.deserializeUser(function(obj, done) {
	done(null, obj);
});

//definimos la estrategia de autenticacion
var estrategiaAutenticacion = new TwitterStrategy({
	consumerKey : TWITTER_CONSUMER_KEY,
	consumerSecret : TWITTER_CONSUMER_SECRET,
	callbackURL : "http://127.0.0.1:3000/auth/twitter/callback",
	passReqToCallback : true
}, function(request, token, tokenSecret, profile, done) {
	process.nextTick(function() {

		console.log("PERFIL USUARIO:" + JSON.stringify(profile));
		//guardamos los datos en session
		request.session.usuarioToken = token;
		request.session.usuarioTokenSecret = tokenSecret;
		request.session.screen_name = profile.username;

		console.log("PROFILE.USERNAME:	" + profile.username);
		return done(null, profile);
	});
});

//configuramos passport con la estrategia
passport.use(estrategiaAutenticacion);

//AHORA SI INICIAMOS LA APLICACION
var app = express();

//CONFIGURACION DE EXRESS
app.configure(function() {
	app.set('views', __dirname + '/views');
	//LE INDICAMOS QUE VAMOS A USAR EJS
	app.set('view engine', 'ejs');
	app.use(express.logger());
	app.use(express.cookieParser());
	app.use(express.json());
	app.use(express.urlencoded());
	app.use(express.methodOverride());
	app.use(express.cookieParser());
	app.use(express.session({
		secret : 'keyboard cat'
	}));

	//Acoplamos express con passport
	app.use(passport.initialize());
	app.use(passport.session());
	app.use(app.router);
	app.use(express.static(__dirname + '/public'));

});

app.get('/', function(req, res) {

	//si el usuario ya dio permiso
	if (req.session.usuarioToken) {

		res.redirect("/bienvenido");

		//si no ha dado permiso lo llevamos al login
	} else {

		res.render("login");

	}
});

//CONSUMIMOS EL API DE TWITTER
app.get('/bienvenido', function(request, res) {

	//obtenemos los datos de la session
	var usuarioAccessToken = request.session.usuarioToken;
	var usuarioTokenSecret = request.session.usuarioTokenSecret;
	var screen_name = request.session.screen_name;
	//mostramos los datos usando el api de twitter
	muestraDatos(usuarioAccessToken, usuarioTokenSecret, screen_name, res);

});

app.get('/account', ensureAuthenticated, function(req, res) {
	res.render('account', {
		user : req.user
	});
});

// GET /auth/twitter
//   Use passport.authenticate() as route middleware to authenticate the
//   request.  The first step in Twitter authentication will involve redirecting
//   the user to twitter.com.  After authorization, the Twitter will redirect
//   the user back to this application at /auth/twitter/callback
app.get('/auth/twitter', passport.authenticate('twitter'));

// GET /auth/twitter/callback
//   Use passport.authenticate() as route middleware to authenticate the
//   request.  If authentication fails, the user will be redirected back to the
//   login page.  Otherwise, the primary route function function will be called,
//   which, in this example, will redirect the user to the home page.
app.get('/auth/twitter/callback', passport.authenticate('twitter', {
	successRedirect : '/bienvenido',
	failureRedirect : '/login'
}));

app.get('/logout', function(req, res) {
	req.logout();
	res.redirect('/');
});

app.listen(3000);

// Simple route middleware to ensure user is authenticated.
//   Use this route middleware on any resource that needs to be protected.  If
//   the request is authenticated (typically via a persistent login session),
//   the request will proceed.  Otherwise, the user will be redirected to the
//   login page.
function ensureAuthenticated(req, res, next) {
	if (req.isAuthenticated()) {
		return next();
	}
	res.redirect('/login')
}
